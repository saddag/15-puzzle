import random
import os
import pygame

assets_folder = os.path.join(os.path.dirname(__file__), 'assets')
WIDTH, HEIGHT = 450, 530
FPS = 60
BG_COLOR = (191, 215, 234)
TEXT_COLOR = (11, 57, 84)
UP, DOWN, RIGHT, LEFT = 'up', 'down', 'right', 'left'
MENU, RESUME, SHUFFLE, TIME, EXIT = 'menu', 'resume', 'shuffle', 'time', 'exit'
IN_MENU, IN_GAME = 'inmenu', 'ingame'
DEFAULT_BOARD = [[1, 5, 9, 13],
                 [2, 6, 10, 14],
                 [3, 7, 11, 15],
                 [4, 8, 12, 16]]


def main():
    pygame.init()
    icon = pygame.image.load(os.path.join(assets_folder, 'ico.dat'))
    pygame.display.set_icon(icon)

    os.environ['SDL_VIDEO_CENTERED'] = '1'

    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption('15 Puzzle')
    clock = pygame.time.Clock()
    state = IN_GAME
    start = False
    time = 0

    background_img = pygame.transform.scale(pygame.image.load(os.path.join(assets_folder, 'background.dat')), (WIDTH, HEIGHT))
    font = pygame.font.Font(os.path.join(assets_folder, 'font.dat'), 25)
    board_img = pygame.image.load(os.path.join(assets_folder, 'board.dat'))
    tiles_img = pygame.image.load(os.path.join(assets_folder, 'tiles.dat'))

    board = board_init(tiles_img, board_img)
    time_gui = GUI(TIME, pygame.Rect(205, 15, 228, 70), text=font.render('00:00:00', 1, TEXT_COLOR))
    win_gui = win_gui_init(font)
    game_guis = guis_init(font)
    menu_guis = menu_guis_init(font)
    clicked_button = None

    while True:
        hrs = time // 3600000
        mins = time // 60000 - hrs * 60
        sec = time // 1000 - mins * 60
        sec = str(sec).zfill(2)
        mins = str(mins).zfill(2)
        hrs = str(hrs).zfill(2)
        win = board.check_win()

        guis = game_guis if state == IN_GAME else menu_guis
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()

            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                mouse_position = event.pos
                tile_clicked = board.get_tiles_clicked(mouse_position)
                if not win and start and tile_clicked and board.check_tile_movable(tile_clicked):
                    board.move_tile(tile_clicked)
                clicked_button = guis.get_button_clicked(mouse_position)
                if clicked_button:
                    clicked_button.clicked()

            if clicked_button and event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                clicked_button.unclicked()
                mouse_position = event.pos
                but = guis.get_button_clicked(mouse_position)
                if but == clicked_button:
                    if clicked_button.gui_type == MENU:
                        state = IN_MENU
                    if clicked_button.gui_type == RESUME:
                        state = IN_GAME
                    if clicked_button.gui_type == SHUFFLE:
                        state = IN_GAME
                        start = True
                        time = 0
                        board.shuffle_board(150)
                    if clicked_button.gui_type == EXIT:
                        pygame.quit()
                        exit()

        board.update()

        screen.blit(background_img, (0, 0))
        board.draw(screen)
        time_gui.text = font.render(hrs + ':' + mins + ':' + sec, 1, TEXT_COLOR)
        time_gui.draw(screen)
        if start and win and state != IN_MENU:
            win_gui.draw(screen)

        guis.draw(screen)

        pygame.display.flip()

        if start and not win and state == IN_GAME:
            time += clock.tick(FPS)
        else:
            clock.tick(FPS)


class GUI:
    def __init__(self, gui_type, rect, fill=BG_COLOR, border=TEXT_COLOR, text=None):
        self.fill_color = fill
        self.border_color = border
        self.gui_type = gui_type
        self.rect = rect
        if text:
            self.text = text
            self.text_rect = pygame.Rect((0, 0, text.get_width(), text.get_height()))
            self.text_rect.center = self.rect.center

    def draw(self, screen):
        pygame.draw.rect(screen, self.fill_color, self.rect)
        pygame.draw.rect(screen, self.border_color, self.rect, 5)
        if self.text:
            screen.blit(self.text, self.text_rect)

    def clicked(self):
        self.fill_color = [i - 60 for i in self.fill_color]

    def unclicked(self):
        self.fill_color = [i + 60 for i in self.fill_color]


class GuiGroup:
    def __init__(self, buttons, guis):
        self.buttons = buttons
        self.guis = guis

    def get_button_clicked(self, mouse_position):
        buttons = self.buttons
        for button in buttons:
            if button.rect.collidepoint(mouse_position):
                return button

    def draw(self, screen):
        buttons, guis = self.buttons, self.guis
        for gui in guis:
            if pygame.Rect(0, 0, WIDTH, HEIGHT).colliderect(gui.rect):
                gui.draw(screen)
        for button in buttons:
            if pygame.Rect(0, 0, WIDTH, HEIGHT).colliderect(button.rect):
                button.draw(screen)


class Tile(pygame.sprite.Sprite):
    def __init__(self, num, x, y, image, rect):
        pygame.sprite.Sprite.__init__(self)
        self.num = num
        self.x, self.y = x, y
        self.image = image
        self.rect = rect


class Board:
    def __init__(self, tiles, image, rect):
        self.tiles = tiles
        self.image = image
        self.rect = rect
        self.moving_tile = None
        self.target_rect = None
        self.speed = 20

        for col in self.tiles:
            for tile in col:
                tile.rect.move_ip(self.rect.x + 10, self.rect.y + 10)

    def draw(self, screen):
        screen.blit(self.image, self.rect)
        for col in self.tiles:
            for tile in col:
                screen.blit(tile.image, tile.rect)

    def get_tiles_clicked(self, mouse_position):
        for col in self.tiles:
            for tile in col:
                if tile.rect.collidepoint(mouse_position):
                    return tile

    def get_zero_tile(self):
        for col in self.tiles:
            for tile in col:
                if tile.num == 16:
                    return tile

    def check_tile_movable(self, tile):
        x, y = tile.x, tile.y
        zero = self.get_zero_tile()
        zx, zy = zero.x, zero.y
        if zx == x:
            if abs(y - zy) == 1:
                return True
        elif zy == y:
            if abs(x - zx) == 1:
                return True
        return False

    def move_tile(self, tile):
        if not self.moving_tile:
            zero = self.get_zero_tile()
            self.target_rect = zero.rect.copy()
            zero.rect = tile.rect.copy()
            self.moving_tile = tile
            tile.x, tile.y, zero.x, zero.y = zero.x, zero.y, tile.x, tile.y

    def swap_tile(self, tile):
        zero = self.get_zero_tile()
        zero.rect, tile.rect = tile.rect, zero.rect
        tile.x, tile.y, zero.x, zero.y = zero.x, zero.y, tile.x, tile.y

    def get_available_directions(self, last_direction=None):
        zero = self.get_zero_tile()
        directions = [UP, RIGHT, LEFT, DOWN]
        if last_direction in directions:
            directions.remove(last_direction)
        if zero.x == 3 and LEFT in directions:
            directions.remove(LEFT)
        if zero.x == 0 and RIGHT in directions:
            directions.remove(RIGHT)
        if zero.y == 3 and UP in directions:
            directions.remove(UP)
        if zero.y == 0 and DOWN in directions:
            directions.remove(DOWN)
        return directions

    def get_tile(self, x, y):
        for col in self.tiles:
            for tile in col:
                if tile.x == x and tile.y == y:
                    return tile

    def get_movable_tile(self, direction):
        zero = self.get_zero_tile()
        zx, zy = zero.x, zero.y
        x, y = 0, 0
        if direction == UP:
            x, y = zx, zy + 1
        if direction == DOWN:
            x, y = zx, zy - 1
        if direction == RIGHT:
            x, y = zx - 1, zy
        if direction == LEFT:
            x, y = zx + 1, zy
        return self.get_tile(x, y)

    def swap_in_direction(self, direction):
        tile = self.get_movable_tile(direction)
        self.swap_tile(tile)

    def get_random_direction(self, last_direction=None):
        directions = self.get_available_directions(last_direction)
        return random.choice(directions)

    def shuffle_board(self, steps=60):
        last_direction = None
        for i in range(steps):
            direction = self.get_random_direction(last_direction)
            self.swap_in_direction(direction)
            last_direction = direction

    def set_default(self):
        for x in range(4):
            for y in range(4):
                self.tiles[x][y].x, self.tiles[x][y].y = x, y
                self.tiles[x][y].rect.topleft = (x * self.tiles[x][y].rect.w + self.rect.x + 10,
                                                 y * self.tiles[x][y].rect.h + self.rect.y + 10)

    def check_win(self):
        for col in self.tiles:
            for tile in col:
                if tile.num != DEFAULT_BOARD[tile.x][tile.y]:
                    return False
        return True

    def update(self):
        if self.moving_tile:
            vx, vy = 0, 0
            if self.target_rect.x > self.moving_tile.rect.x:
                vx = self.speed
            elif self.target_rect.x < self.moving_tile.rect.x:
                vx = -self.speed
            if self.target_rect.y > self.moving_tile.rect.y:
                vy = self.speed
            elif self.target_rect.y < self.moving_tile.rect.y:
                vy = -self.speed
            self.moving_tile.rect.move_ip(vx, vy)
            if self.moving_tile.rect.x == self.target_rect.x and self.moving_tile.rect.y == self.target_rect.y:
                self.moving_tile = None
                self.target_rect = None


def board_init(tiles_image, board_image):
    board_rect = pygame.Rect(0, 0, board_image.get_width(), board_image.get_height())
    board_rect.centerx = WIDTH / 2
    board_rect.bottom = HEIGHT - 15
    return Board(tiles_init(tiles_image), board_image, board_rect)


def tiles_init(tiles_image):
    tiles = []
    tile_w = tiles_image.get_width() // 4
    tile_h = tiles_image.get_height() // 4
    for x in range(4):
        col = []
        num = x + 1
        for y in range(4):
            img_rect = (x * tile_w, y * tile_h, tile_w, tile_h)
            img = tiles_image.subsurface(img_rect)
            rect = pygame.Rect(img_rect)
            col.append(Tile(num, x, y, img, rect))
            num += 4
        tiles.append(col)
    return tiles


def menu_guis_init(font):
    gui_rect = pygame.Rect(15, 95, WIDTH - 30, HEIGHT - 110)
    guis = [GUI(None, gui_rect, fill=(170, 195, 214), text=font.render('', 1, TEXT_COLOR))]
    buttony = gui_rect.centery - 80
    buttonx = gui_rect.centerx - 150
    buttons = [GUI(RESUME, pygame.Rect(15, 15, 180, 70), text=font.render('Продолжить', 1, TEXT_COLOR)),
               GUI(SHUFFLE, pygame.Rect(buttonx, buttony - 40, 300, 70), text=font.render('Новая игра', 1, TEXT_COLOR)),
               GUI(EXIT, pygame.Rect(buttonx, buttony + 80, 300, 70), text=font.render('Выход', 1, TEXT_COLOR))]
    return GuiGroup(buttons, guis)


def guis_init(font):
    buttons = [GUI(MENU, pygame.Rect(15, 15, 180, 70), text=font.render('Меню', 1, TEXT_COLOR))]
    guis = []
    return GuiGroup(buttons, guis)


def win_gui_init(font):
    win_rect = pygame.Rect(0, 0, 300, 100)
    win_rect.center = (WIDTH / 2, HEIGHT / 2)
    return GUI('WIN', win_rect, fill=(211, 235, 254), text=font.render('Победа!', 1, TEXT_COLOR))


if __name__ == '__main__':
    main()
